from setuptools import setup, find_packages

# Setting up
setup(
    name="lab10_python_pip_jpelc",
    packages=find_packages(),
    install_requires=['requests']
)